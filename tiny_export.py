import codecs
import os
import sys
import sqlite3
import math
from lxml import etree

# kicad lib utils
common = os.path.abspath(os.path.join(sys.path[0], 'common'))
if not common in sys.path:
    sys.path.append(common)

import kicad_schematic
from  kicad_sym import *


# up, down, left, right
tinycad_to_kicad_rotation = [90,270,180,0]

def get_point (str):
    pt = str.split(',') 
    return Point (float(pt[0]), float(pt[1]))


def read_lib (filename):
    conn = sqlite3.connect(filename)

    conn.row_factory = sqlite3.Row
    c = conn.cursor()

    query = 'SELECT * FROM [Name] WHERE [Type]=0'
    c.execute (query)
    names = c.fetchall()

    print (names[0].keys())

    for row in names:
        print (tuple(row))

        c.execute ('SELECT [Data] FROM [Symbol] WHERE [SymbolId]={}'.format(row['SymbolId']))
        data = c.fetchall()
        #print (data[0]['Data'])

        with open("{}.xml".format(row['SymbolId']), "w") as f:
            d = data[0]['Data']
            raw = codecs.escape_decode (d)[0].decode ("utf-8")
            f.write (raw)

def make_line (start, end):
    pts = []
    pts.append (start)
    pts.append (end)

    return Polyline(pts)


def convert_symbol_base (sym, xml):

    for child in xml.xpath("ELLIPSE"):
        start = get_point(child.get("a"))
        end = get_point(child.get("b")) 

        radius = math.fabs(start.x-end.x)/2
        pos = Point(0,0)
        pos.x = (start.x+end.x)/2
        pos.y = (start.y+end.y)/2

        sym.circles.append (Circle (pos.x, -pos.y, radius, 0.15))

    for child in xml.xpath("POLYGON"):
        origin = get_point(child.get("pos")) 
        # style, fill
        pts = []
        for pt in child.xpath("POINT"):
            ## todo : arc
            p = get_point(pt.get("pos"))
            p.x += origin.x
            p.y += origin.y
            p.y = -p.y
            pts.append (p)

        sym.polylines.append (Polyline(pts))


    for child in xml.xpath("PIN"):
        pos = get_point(child.get("pos")) 

        attribs = {}
        for key in ['which', 'elec', 'direction', 'part', 'show', 'length', 'number_pos']:
            val = int(child.get(key))
            attribs[key]=val

        if child.text:
            name = child.text
        else:
            name = ""
        num = child.get("number")
        pin_length = attribs['length']/5.0

        if False:
            if attribs['direction'] == 0:
                pos.y -= pin_length
            elif attribs['direction'] == 1:
                pos.y += pin_length
            elif attribs['direction'] == 2:
                pos.x -= pin_length
            elif attribs['direction'] == 3:
                pos.x += pin_length

        pin_shape = 'line'

        if attribs['elec'] == 0:
            pin_elec_type = 'input'
        elif attribs['elec'] == 1:
            pin_elec_type = 'output'
        elif attribs['elec'] == 2:
            pin_elec_type = 'tri_state'
        elif attribs['elec'] == 3:
            pin_elec_type = 'open_collector'
        elif attribs['elec'] == 4:
            pin_elec_type = 'passive'
        elif attribs['elec'] == 5:
            pin_elec_type = 'bidirectional'
        elif attribs['elec'] == 6:
            pin_elec_type = 'no_connect'
        else:
            pin_elec_type = 'passive'

        pin = Pin(name, num, pin_elec_type, pos.x, -pos.y, tinycad_to_kicad_rotation[attribs['direction']], pin_shape, pin_length)
        pin.unit = attribs['part']+1
        # 
        if attribs['which'] == 5:
            pin.is_hidden = True

        if (attribs['show'] & 1) == 0:
            pin.name_effect.is_hidden = True
            sym.hide_pin_names = True

        if (attribs['show'] & 2) == 0:
            pin.number_effect.is_hidden = True
            sym.hide_pin_numbers = True

        sym.pins.append (pin)

def convert_symbol_from_schema (xml):

    name = xml.xpath("NAME")[0].text
    ref = xml.xpath("REF")[0].text

    # what to use for lib name?
    sym = KicadSymbol.new(name, "project", ref)

    # properties
    # ppp= unit ?
    sym.unit_count = 1

    convert_symbol_base (sym, xml.xpath("TinyCAD")[0])

    return sym


class Converter:
    def convert_schematic (self, filename):

        sch = kicad_schematic.KicadSchematic()

        sch.filename = "test.kicad_sch"

        power_symbols = {}

        self.tree = etree.parse(filename)
        self.root = self.tree.getroot()

        for child in self.root.xpath("WIRE"):
            start = child.get("a").split(',') 
            end = child.get("b").split(',') 
            sch.add_wire (start, end, 0.15)

        for child in self.root.xpath("JUNCTION"):
            pos = child.get("pos").split(',') 
            sch.add_junction (pos, 1)

        for child in self.root.xpath("SYMBOLDEF"):
            sym = convert_symbol_from_schema (child)
            sch.add_lib_symbol (sym)

        for child in self.root.xpath("SYMBOL"):
            id = int(child.get("id")) - 1
            pos = get_point(child.get("pos"))
            unit = int(child.get("part")) + 1
            rotate = int(child.get("rotate"))
            #rotate = tinycad_to_kicad_rotation[rotate]
            rotate = rotate * 90

            instance = kicad_schematic.SymbolInstance (sch.lib_symbols[id], pos, rotate, unit)
            sch.symbol_instances.append (instance)

            for field in child.xpath("FIELD"):
                type = field.get("type")
                description = field.get("description")
                value = field.get("value")
                show  = int(field.get("show"))
                field_pos = get_point(field.get("pos"))

                prop = None
                if description == "Ref":
                    prop = instance.get_property ("Reference")
                elif description == "Name":
                    prop = instance.get_property ("Value")
                elif description == "Package":
                    prop = instance.get_property ("Footprint")
                elif show == 1:
                    prop = Property (description, "", len(instance.properties))
                    instance.properties.append (prop)

                if prop:
                    prop.value = value
                    prop.posx = field_pos.x 
                    prop.posy = field_pos.y 
                    prop.effects.is_hidden = (show == 0)

        for child in self.root.xpath("POWER"):
            pos = get_point(child.get("pos"))
            which = int(child.get("which"))
            direction = int(child.get("direction"))

            # find power sym
            key = child.text
            power_sym = None
            if key in power_symbols:
                power_sym = power_symbols[key]
            else:
                # if not exist, create it
                name = child.text
                ref = '#PWR'
                # what to use for lib name?
                power_sym = KicadSymbol.new(name, "project", ref)
                power_sym.get_property ("Value").posy = -4
                power_sym.get_property ("Reference").effects.is_hidden = True
                power_sym.unit_count = 1
                #
                pin = Pin(name, "", 'power_in', 0, 0, 90, 'line', 0)
                pin.unit = 1
                pin.is_hidden = True
                power_sym.hide_pin_names = True
                power_sym.hide_pin_numbers = True
                power_sym.pins.append (pin)
                power_sym.is_power = True

                power_sym.polylines.append (make_line (Point (0,0), Point(0,3)))
                power_sym.polylines.append (make_line (Point (-1,3), Point(1,3)))

                sch.add_lib_symbol (power_sym)

            # add reference to power sym
            id = len(sch.lib_symbols)-1
            instance = kicad_schematic.SymbolInstance (sch.lib_symbols[id], pos, direction*180, 1)
            instance.get_property ("Reference").effects.is_hidden = True
            instance.get_property ("Value").posy = -4
            sch.symbol_instances.append (instance)
        #
        sch.save()

if __name__ == "__main__":

    if False:
        lib_path = "C:/github/TinyCAD/examples/libraries"
        #lib = "74TTL.TCLib"
        #lib = "symbols.TCLib"
        lib = "Connectors.TCLib"
        read_lib (os.path.join(lib_path, lib))

    converter = Converter()
    converter.convert_schematic ("C:/github/TinyCAD/examples/circuits/AMP.DSN")