#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

import sexpr
from boundingbox import BoundingBox

from kicad_sym import Point, Property, KicadSymbol

class SymbolInstance(object):

    def __init__(self, sym_ref, pos, rotation=0, unit=1):
        self.symbol_ref = sym_ref
        self.pos = pos
        self.rotation = rotation
        self.unit_num = unit

        self.lib_id = "{}:{}".format(sym_ref.libname, sym_ref.name)

        self.in_bom = sym_ref.in_bom
        self.on_board = sym_ref.on_board
        self.properties = []
        self.add_default_properties()

        for property_name in ["Reference", "Value", "Footprint", "Datasheet"]:
            prop = sym_ref.get_property(property_name)
            if prop:
                self.get_property (property_name).value = prop.value


    def get_property(self, pname):
        for p in self.properties:
            if p.name == pname:
                return p
        return None

    def add_default_properties(self):
        defaults = [
            {'i': 0, 'n': "Reference", 'v': "", 'h': False},
            {'i': 1, 'n': "Value", 'v': "", 'h': False},
            {'i': 2, 'n': "Footprint", 'v': "", 'h': True},
            {'i': 3, 'n': "Datasheet", 'v': "", 'h': True}
        ]

        for prop in defaults:
            if self.get_property(prop['n']) == None:
                p = Property(prop['n'], prop['v'], prop['i'])
                p.effects.is_hidden = prop['h']
                self.properties.append(p)



class KicadSchematic(object):
    """
    A class to encapsulate kicad_sch files
    """
    def __init__(self, filename=None):

        self.filename = filename

        self.version = 20201015 # v5.99
        self.generator = "kicad_schematic"

        # general

        self.paper = "A4"
        self.wires = []
        self.junctions = []
        self.polylines = []
        self.lib_symbols = []
        self.symbol_instances = []


    def from_file(self, filename):
        pass


    def get_sexpr (self):
        pass

    def add_wire(self, start, end, width, fill='solid'):
        points = []
        points.append({'x':start[0], 'y':start[1]})
        points.append({'x':end[0], 'y':end[1]})

        line={
               'pts': points,
               'width': width,
               'fill': fill
             }
        self.wires.append(line)

    def add_polyline(self, pts, width, fill='solid'):
        points = []
        for pt in pts:
            points.append({'x':pt[0], 'y':pt[1]})
        
        poly={
               'pts': points,
               'width': width,
               'fill': fill
             }
        self.polylines.append(poly)


    def add_junction(self, pos, size):
        junction={
               'pos': {'x':pos[0], 'y':pos[1]},
               'size': size
             }
        self.junctions.append(junction)

    def add_lib_symbol (self, symbol):
        self.lib_symbols.append (symbol)

    def _formatWire(self, wire, se):
        se.startGroup('wire', newline=True, indent=False)

        pts = []
        for p in wire['pts']:
            pts.append ({'xy':[ p['x'], p['y'] ] })

        se.startGroup('pts', newline=True, indent=True)
        sch_poly = [ pts ]
        for p in pts:
            se.addItem(p, newline=True)
        se.endGroup(newline=True)

        se.startGroup('stroke', newline=True, indent=True)
        stroke = [ {'width': wire['width']},
                   {'type': wire['fill']}
                 ]
        se.addItem(stroke, newline=True)
        se.endGroup(newline=True)


        se.endGroup(newline=False)

    def _formatPolyline(self, poly, se):
        se.startGroup('polyline', newline=True, indent=False)

        pts = []
        for p in poly['pts']:
            pts.append ({'xy':[ p['x'], p['y'] ] })

        se.startGroup('pts', newline=True, indent=True)
        sch_poly = [ pts ]
        for p in pts:
            se.addItem(p, newline=True)
        se.endGroup(newline=True)

        sch_poly = [
            {'width': poly['width']},
            {'fill': poly.get('fill', 'solid')}
            ]

        se.addItems(sch_poly, newline=False)
        se.endGroup(newline=False)

    def _formatJunction(self, junction, se):
        se.startGroup('junction', newline=True, indent=False)

        data = [
            {'at': [junction['pos']['x'], junction['pos']['y']]},
            {'diameter' : junction['size']}
            ]

        se.addItems(data, newline=False)
        se.endGroup(newline=False)

    def _format_instance(self, instance, se):
        se.startGroup('symbol', newline=True, indent=False)

        data = [
            #{'lib_name': instance.lib_id},
            {'lib_id': instance.lib_id },
            {'at': [instance.pos.x, instance.pos.y, instance.rotation]} ,
            {'unit' : instance.unit_num},
            {'in_bom' : 'yes' if instance.in_bom else 'no'},
            {'on_board' : 'yes' if instance.on_board else 'no'}
            # uuid
            ]

        # add properties
        for prop in instance.properties:
            se.output += sexpr.format_sexp(sexpr.build_sexp(prop.get_sexpr()))

        se.addItems(data, newline=False)
        se.endGroup(newline=False)

    def save(self, filename=None):
        if not filename:
            filename = self.filename

        se = sexpr.SexprBuilder('kicad_sch')

        # Hex value of current epoch timestamp (in seconds)
        tedit = hex(int(time.time())).upper()[2:]

        # Add header items
        header = []
        header.append({'version': self.version})
        header.append({'generator': self.generator})
        #header.append ({'host': ['pcbnew', '(5.1.2)-2']})
        se.addItems(header, newline=False)

        # lib_symbols
        se.startGroup('lib_symbols', newline=True, indent=True)
        for sym in self.lib_symbols:
            se.output += sexpr.format_sexp(sexpr.build_sexp(sym.get_sexpr()))
        se.endGroup(newline=True)


        for junction in self.junctions:
            self._formatJunction (junction, se)

        for wire in self.wires:
            self._formatWire (wire, se)

        # symbol/uni instances
        for instance in self.symbol_instances:
            self._format_instance (instance, se)

        # The End
        se.endGroup(True)

        with open(filename, 'w', newline='\n') as f:
            f.write(se.output)
            f.write('\n')
