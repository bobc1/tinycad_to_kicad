#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

import sexpr
from boundingbox import BoundingBox


class KicadPcb(object):
    """
    A class to encapsulate kicad_pcb files
    """
    def __init__(self, filename=None):

        self.filename = filename

        #self.version = 20210108 # v5.99
        self.version = 20171130 # v5.1.9
        self.generator = "kicad_pcb"

        # general
        self.thickness = 1.6

        self.paper = "A4"

        # layers
        self.layers = {
             "0": ["F.Cu",'signal'],
            "31": ["B.Cu", 'signal'],
            "32": ["B.Adhes", 'user'],
            "33": ["F.Adhes", 'user'],
            "34": ["B.Paste", 'user'],
            "35": ["F.Paste", 'user'],
            "36": ["B.SilkS", 'user'],
            "37": ["F.SilkS", 'user'],
            "38": ["B.Mask", 'user'],
            "39": ["F.Mask", 'user'],
            "40": ["Dwgs.User", 'user'],
            "41": ["Cmts.User", 'user'],
            "42": ["Eco1.User", 'user'],
            "43": ["Eco2.User", 'user'],
            "44": ["Edge.Cuts", 'user'],
            "45": ["Margin", 'user'],
            "46": ["B.CrtYd", 'user'],
            "47": ["F.CrtYd", 'user'],
            "48": ["B.Fab", 'user'],
            "49": ["F.Fab", 'user']
            }
        # setup

        # nets
        self.nets = {"0": ""}

        # net classes

        # footprints

        # gr_rect / gr_poly
        self.polys = []

        # gr_text
        self.texts = []

        # gr_line
        self.lines = []

        # gr_circle

        # dimensions

        # vias
        self.vias = []

        # segment
        self.segment= []

    def from_file(self, filename):
        pass


    def get_sexpr (self):
        pass

    def addLine(self, start, end, layer, width):
        line={
               'start': {'x': start[0], 'y': start[1]},
               'end': {'x': end[0], 'y': end[1]},
               'layer': layer,
               'width': width
             }
        self.lines.append(line)

    def addPoly(self, pts, layer, width, fill='solid'):
        points = []
        for pt in pts:
            points.append({'x':pt[0], 'y':pt[1]})
        
        poly={
               'pts': points,
               'layer': layer,
               'width': width,
               'fill': fill
             }
        self.polys.append(poly)

    def addVia (self, pos, size, drill):
        via = {
            'pos': {'x':pos[0], 'y':pos[1]},
            'size': size,
            'drill': drill,
            'layers': ["F.Cu", "B.Cu"],
            'free': False,
            'net': "0"
            }
        self.vias.append (via)

    def _formatLine(self, line, se):
        se.startGroup('gr_line', newline=True, indent=False)

        start = line['start']
        end = line['end']

        fp_line = [
            {'start': [start['x'], start['y']]},
            {'end'  : [end['x'], end['y']]},
            {'layer': line['layer']},
            {'width': line['width']}
            ]

        se.addItems(fp_line, newline=False)
        se.endGroup(newline=False)

    def _formatPoly(self, poly, se):
        se.startGroup('gr_poly', newline=True, indent=False)

        pts = []
        for p in poly['pts']:
            pts.append ({'xy':[ p['x'], p['y'] ] })

        se.startGroup('pts', newline=True, indent=True)
        fp_poly = [ pts ]
        for p in pts:
            se.addItem(p, newline=True)
        se.endGroup(newline=True)

        fp_poly = [
            {'layer': poly['layer']},
            {'width': poly['width']}
            ]

        if self.version > 20201115:
            fp_poly.append ({'fill': poly.get('fill', 'solid')})

        se.addItems(fp_poly, newline=False)
        se.endGroup(newline=False)

    def _formatVia(self, via, se):
        # (via (at 125 100) (size 0.6) (drill 0.4) (layers "F.Cu" "B.Cu") (free) (net 0) (tstamp 3c7c0b88-6e97-4781-8a9a-bd3e16097ba8))

        se.startGroup('via', newline=True, indent=False)

        data = [
            {'at': [via['pos']['x'], via['pos']['y']]},
            {'size' : via['size']},
            {'drill' : via['drill']},
            {'layers': via['layers']}
            ]

        if via['free']:
            data.append ("free")

        data.append ({"net": via['net']})

        se.addItems(data, newline=False)
        se.endGroup(newline=False)

    def save(self, filename=None):
        if not filename:
            filename = self.filename

        se = sexpr.SexprBuilder('kicad_pcb')

        # Hex value of current epoch timestamp (in seconds)
        tedit = hex(int(time.time())).upper()[2:]

        # Add header items
        header = []
        header.append({'version': self.version})
        #root.append({'generator': self.generator})
        header.append ({'host': ['pcbnew', '(5.1.2)-2']})
        se.addItems(header, newline=False)

        # Add Layers
        se.startGroup('layers');
        se.addItems (self.layers)
        se.endGroup()

        # Add Nets
        for key in self.nets.keys():
            se.addItem ({'net': [ key, self.nets[key]]})

        # Add Line Data
        for line in self.lines:
            self._formatLine (line, se)

        # Add Poly Data
        for poly in self.polys:
            self._formatPoly(poly, se)

        # Add Vias
        for via in self.vias:
            self._formatVia(via, se)


        se.endGroup(True)

        with open(filename, 'w', newline='\n') as f:
            f.write(se.output)
            f.write('\n')
