#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

class BoundingBox(object):
    
    xmin = None
    ymin = None
    xmax = None
    ymax = None

    def __init__(self, xmin=None, ymin=None, xmax=None, ymax=None):
        self.addPoint(xmin, ymin)
        self.addPoint(xmax, ymax)
        
    def checkMin(self, current, compare):
        if current == None:
            return compare
            
        if compare == None:
            return current
        
        if compare < current:
            return compare
            
        return current
        
    def checkMax(self, current, compare):
        if current == None:
            return compare
            
        if compare == None:
            return current
            
        if compare > current:
            return compare
            
        return current
        
    def addPoint(self, x, y, radius=0):
        # x might be 'None' so prevent subtraction 
        self.xmin = self.checkMin(self.xmin, x - radius if x else x)
        self.xmax = self.checkMax(self.xmax, x + radius if x else x)
        
        # y might be 'None' so prevent subtraction
        self.ymin = self.checkMin(self.ymin, y - radius if y else y)
        self.ymax = self.checkMax(self.ymax, y + radius if y else y)
        
    def addBoundingBox(self, other):
        self.addPoint(other.xmin, other.ymin)
        self.addPoint(other.xmax, other.ymax)
        
    @property
    def valid(self):
        if self.xmin is None or self.ymin is None or self.xmax is None or self.ymax is None:
            return False
        return True
        
    def containsPoint(self, x, y):
        if not self.valid:
            return False
            
        if x < self.xmin or self.xmax < x:
            return False
            
        if y < self.ymin or self.ymax < y:
            return False
            
        return True
        
    def expand(self, distance):
        if not self.valid:
            return
        self.xmin -= distance
        self.ymin -= distance
        
        self.xmax += distance
        self.ymax += distance
        
    def overlaps(self, other):
        return any([
            self.containsPoint(other.xmin, other.ymin),
            self.containsPoint(other.xmin, other.ymax),
            self.containsPoint(other.xmax, other.ymax),
            self.containsPoint(other.xmax, other.ymin)
            ])
            
    @property
    def x(self):
        return self.xmin
        
    @property
    def y(self):
        return self.ymin
            
    @property
    def width(self):
        if self.xmax is None or self.xmin is None:
            return None
        return self.xmax - self.xmin
        
    @property
    def height(self):
        if self.ymax is None or self.ymin is None:
            return None
        return self.ymax - self.ymin
        
    @property
    def size(self):
        res = {}
        if self.width is not None:
            res['x'] = self.width
        if self.height is not None:
            res['y'] = self.height
        return res
        
    @property
    def center(self):
        res = {}
        if self.width is not None:
            res ['x'] = self.xmin + self.width / 2
        if self.height is not None:
            res ['y'] = self.ymin + self.height / 2
        return res

    def __str__(self):
        return "({}, {})-({}, {}) ({}, {})".format(
            self.xmin if self.xmin is not None else "nul",
            self.ymin if self.ymin is not None else "nul",
            self.xmax if self.xmax is not None else "nul",
            self.ymax if self.ymax is not None else "nul",
            round(self.width,4) if self.width else "nul",
            round(self.height,4) if self.height else "nul")

    def __repr__(self):
        return self.__str__()
            
if __name__ == '__main__':
    def dump (bb):
        print("valid {} w {} h {} size {} center {}".format(bb.valid, bb.width, bb.height, bb.size, bb.center))

    bb1 = BoundingBox()
    bb2 = BoundingBox(2,200)
    bb3 = BoundingBox(-5,-5,7,21)

    bb4 = BoundingBox(xmin=2,xmax=200)
    bb5 = BoundingBox(ymin=2,ymax=200)

    for bb in [bb1,bb2,bb3,bb4,bb5]:
        dump(bb)

    print ("-"*80)

    bb1.addPoint(0,0)
    bb1.addPoint(1,1)

    bb2.addBoundingBox(bb3)
    bb4.addBoundingBox(bb3)

    bb5.addPoint(3,5)

    for bb in [bb1,bb2,bb3,bb4,bb5]:
        dump(bb)
    
            