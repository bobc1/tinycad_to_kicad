# tinycad_to_kicad

A TinyCad to KiCad converter.

This program can convert TinyCad schematics and symbol libraries to KiCad format.

# Status

Work in progress. Currently only a bare minimum of features are converted.

# Prerequisites

To run this program, you will need Python 3.6 or later.
 

# Usage

`python tiny_export <tinycad_lib.TClib>`

`python tiny_export <tinycad.sch>`

